
Template.note.events({
	'click .delete-note'() {
		Notes.remove(this._id);
		return false;
	},
	'submit .edit-item'() {
		event.preventDefault();
		const target = event.target[0];
		const text = target.value;

		Notes.update(this._id, {
			$set: { text: text },
		});

		return false;
	}
});