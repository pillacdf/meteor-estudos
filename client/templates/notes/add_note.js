
import { Notes } from '../../../lib/collections.js';

Template.add.events({
	'submit .add-note': function(){
		event.preventDefault();

		const target = event.target[0];
		const text = target.value;

		Notes.insert({
			text: text,
			createdAt: new Date(),
			owner: Meteor.userId(),
			username: Meteor.user().username,
		});

        target.value = '';
        
        toast('Nota adicionada')

		return false;
	}
});

